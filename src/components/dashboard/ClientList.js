import { DeleteOutlined, EditOutlined, SearchOutlined } from "@ant-design/icons";
import { Modal, Table, Tooltip, Form, Input, Space, Button } from 'antd';
import { useState } from "react";
import { connect } from "react-redux";
import { removeClientAction } from "../../store/actions/clientActions";
import Highlighter from 'react-highlight-words';
import ExportJsonExcel from 'js-export-excel';

const ClientList = ({ client, removeClient }) => {
    const [viewDelete, setViewDelete] = useState(false);
    const [deleteId, setDeleteId] = useState();

    const removeClientModal = (id) => {
        setViewDelete(true);
        setDeleteId(id);
    }

    const editClient = (id) => {
        window.location.href = "/client/" + id;
    }

    const handleDeleteCancel = () => {
        setViewDelete(false);
    }

    const deleteClient = () => {
        removeClient(deleteId);
        setViewDelete(false);
    }

    const [state, setState] = useState({
        searchText: '',
        searchedColumn: '',
    })

    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    // ref={node => {
                    //     this.searchInput = node;
                    // }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
          </Button>
                    <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
          </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                // setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                    text
                ),
    });

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    const handleReset = clearFilters => {
        clearFilters();
        setState({ searchText: '' });
    };

    const columns = [
        {
            title: (
                <>
                    <b>Name</b>
                </>
            ),
            dataIndex: "name",
            key: "name",
            ...getColumnSearchProps('name'),
        },
        {
            title: (
                <>
                    <b>Category</b>
                </>
            ),
            dataIndex: "category",
            key: "category",
            ...getColumnSearchProps('category'),
        },
        {
            title: (
                <>
                    <b>Shop</b>
                </>
            ),
            dataIndex: "shop",
            key: "shop",
            ...getColumnSearchProps('shop'),
        },
        {
            title: (
                <>
                    <b>Address</b>
                </>
            ),
            dataIndex: "address",
            key: "address",
            ...getColumnSearchProps('address'),
        },
        {
            title: (
                <>
                    <b>Contact</b>
                </>
            ),
            dataIndex: "contact",
            key: "contact",
            ...getColumnSearchProps('contact'),
        },
        {
            title: (
                <>
                    <b>Mail</b>
                </>
            ),
            dataIndex: "mail",
            key: "mail",
            ...getColumnSearchProps('mail'),
        },
        {
            title: (
                <>
                    <b>Action</b>
                </>
            ),
            dataIndex: "id",
            width: 120,
            key: "id",
            render: (record) => {
                return (
                    <div>
                        <Tooltip title="Delete">
                            <DeleteOutlined
                                style={{ fontSize: 20 }}
                                onClick={() => removeClientModal(record)}
                            />
                        </Tooltip>
                        <Tooltip title="Edit">
                            <EditOutlined
                                style={{ fontSize: 20, marginLeft: 20 }}
                                onClick={() => editClient(record)}
                            />
                        </Tooltip>
                        <Modal
                            title="Client Delete"
                            visible={viewDelete}
                            onCancel={handleDeleteCancel}
                            okText="Yes"
                            cancelText="No"
                            onOk={() => {
                                deleteClient();
                            }}
                        >
                            <Form>
                                <div style={{ textAlign: "center" }}>
                                    <h1>Are you sure to delete this client?</h1>
                                </div>
                            </Form>
                        </Modal>
                    </div>
                );
            },
        },
    ];


    const downloadExcel = () => {
        var option = {};
        let dataTable = [];
        if (client) {
            for (let i in client) {
                if (client) {
                    let obj = {
                        'Name': client[i].name,
                        'Category': client[i].category,
                        'Shop Name': client[i].shop,
                        'Shop Address': client[i].address,
                        'Contact': client[i].contact,
                        'Gmail Id': client[i].mail,
                    }
                    dataTable.push(obj);
                }
            }
        }
        option.fileName = 'Client Information'
        option.datas = [
            {
                sheetData: dataTable,
                sheetName: 'sheet',
                sheetFilter: ['Name', 'Category', 'Shop Name', 'Shop Address', 'Contact', 'Gmail Id'],
                sheetHeader: ['Name', 'Category', 'Shop Name', 'Shop Address', 'Contact', "Gmail Id"],
            }
        ];

        var toExcel = new ExportJsonExcel(option);
        toExcel.saveExcel();
    }

    return (
        <div>
            <Table
                columns={columns}
                dataSource={client}
                style={{ overflow: "auto" }}
            />

            <Button type="primary" style={{ marginLeft: 15 }} onClick={downloadExcel}>Export In Excel</Button>
        </div>

    )
};

const mapDispatchToProps = dispatch => {
    return {
        removeClient: id => dispatch(removeClientAction(id))
    };
};

export default connect(null, mapDispatchToProps)(ClientList);